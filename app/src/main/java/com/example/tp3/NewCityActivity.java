package com.example.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    private WeatherDbHelper db;
    private City c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);
        db = new WeatherDbHelper(this);
        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                c = new City();
                c.setName(textName.getText().toString());
                c.setCountry(textCountry.getText().toString());
                db.addCity(c);
            }
        });

    }


}
